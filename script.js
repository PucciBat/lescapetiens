
//Horloge
function date_heure(id)
{
        var myClock = document.getElementById(id);
        myClock.innerHTML = getClock(myClock);
        setInterval('date_heure("'+id+'");','1000');
        return true;
}

function changeColor (minute, elem){
        var yellow = "colorHorlogeYellow";
        var blue = "colorHorlogeBlue";
        if ( (minute % 2) == 0) {
                elem.classList.remove(blue);
                elem.classList.add(yellow);
        } 
        else {
                elem.classList.remove(yellow);
                elem.classList.add(blue);
        }
        first = false;
}

function getClock (myClock){
        var date = new Date;
        var h = date.getHours();
        if(h<10)
        {
                h = "0"+h;
        }
        var m = date.getMinutes();
        changeColor(m, myClock);
        if(m<10)
        {
                m = "0"+m;
        }
        var s = date.getSeconds();
        if(s<10)
        {
                s = "0"+s;
        }
        return h+':'+m+':'+s;
}


//Lien actif
let btnContainer = document.getElementById("menu");

let btns = document.getElementsByClassName("btn");

for (let i = 0; i < btns.length; i++) {
   btns[i].addEventListener("click", function(){
       let current = document.getElementsByClassName("active");
       current[0].className = current[0].className.replace(" active", "");
       this.className += " active";
   });
}


//Sticky menu
window.onscroll = function() {myFunction()};

var navbar = document.getElementById("menu");
var sticky = navbar.offsetTop;

function myFunction() {
  if (window.pageYOffset >= sticky) {
    navbar.classList.add("sticky")
  } else {
    navbar.classList.remove("sticky");
  }
}

//Diaporama manuel

var slideIndexM = 1;
showMSlides(slideIndexM);

function plusSlides(n) {
        showMSlides(slideIndexM += n);
}

function currentSlide(n) {
        showMSlides(slideIndexM = n);
}

function showMSlides(n) {
        var i;
        var slides = document.getElementsByClassName("myManuSlides");
        if (n > slides.length) { slideIndexM = 1 }
        if (n < 1) { slideIndexM = slides.length }
        for (i = 0; i < slides.length; i++) {
                slides[i].style.display = "none";
        }
        slides[slideIndexM - 1].style.display = "block";
}

//Diaporama automatique
var slideIndex = 0;
showSlides();

function showSlides() {
        var i;
        var slides = document.getElementsByClassName("mySlides");
        for (i = 0; i < slides.length; i++) {
                slides[i].style.display = "none";
        }
        slideIndex++;
        if (slideIndex > slides.length) { slideIndex = 1 };
        slides[slideIndex - 1].style.display = "block";
        setTimeout(showSlides, 2000);
}

